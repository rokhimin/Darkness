<a href="https://github.com/rokhimin/Darkness"><img src="https://media.tenor.com/images/0a3605d63dcca393d5b50f78e3cc87b0/tenor.gif" width="250" align="right"/></a>
## About
Darkness jekyll

###### Live
- host github : https://rokhimin.github.io
- host heroku : https://rokhimin-darkness.herokuapp.com
- host netlify : https://rokhimin-darkness.netlify.com

## Deploy

###### Heroku
[![Deploy to heroku](https://www.herokucdn.com/deploy/button.png)](https://dashboard.heroku.com/new?button-url=https://github.com/rokhimin/Darkness/tree/deploy_heroku&template=https://github.com/rokhimin/Darkness/tree/deploy_heroku) 

###### Netlify
 [![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/rokhimin/jekyll-netlify)

## Test Locally
- ``bundle install``
- ``jekyll s``

## License 
MIT License.
